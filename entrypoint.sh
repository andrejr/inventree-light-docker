#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

FIRST_RUN=0
# if the database folder is empty, this is the first run
if ! [ "$(ls -A "$INVENTREE_PERSIST/db")" ]; then
	FIRST_RUN=1
fi

# check if secret key exists, generate if not.
if ! [ -f "$INVENTREE_PERSIST/secret/key" ]; then
	echo "Generating security key..."

	mkdir -p "$INVENTREE_PERSIST/secret/"
	"$INVENTREE_ROOT/generate_secret_key.py" >"$INVENTREE_PERSIST/secret/key"
fi

cd "$INVENTREE_ROOT/InvenTree"

if [ "${MIGRATE_ON_START:-1}" -ne 0 ] || [ "$FIRST_RUN" -ne 0 ]; then

	mkdir -p "$INVENTREE_PERSIST/media"
	mkdir -p "$INVENTREE_PERSIST/static"
	mkdir -p "$INVENTREE_PERSIST/static_i18n"
	mkdir -p "$INVENTREE_PERSIST/db"
	echo 'Running migrations and generating static files.'
	./manage.py makemigrations
	./manage.py migrate --noinput
	./manage.py migrate --run-syncdb
    ./manage.py prerender
	./manage.py collectstatic --noinput
	./manage.py clearsessions
	echo "Done running migrations and generating static files."
fi

if [ "$FIRST_RUN" -ne 0 ]; then
	echo "First run..."
	echo "Creating superuser"
	./manage.py createsuperuser --noinput \
		--username "$ADMIN_USERNAME" \
		--email "$ADMIN_EMAIL"
fi

# run gunicorn
gunicorn -c gunicorn.conf.py \
	--workers "${GUNICORN_WORKERS:-3}" \
	--bind unix:/home/inventree/inventree.sock \
	InvenTree.wsgi &

if [ -z "${SERVER_NAME}" ]; then
	export SERVER_NAME_STRING="server_name $SERVER_NAME;"
fi

mkdir /home/inventree/nginx || true
# run nginx rootlessly

cd "$INVENTREE_ROOT"
#shellcheck disable=SC2016
envsubst '$LISTENING_PORT $SERVER_NAME_STRING' <nginx.conf.template >|nginx.conf
nginx -c nginx.conf -p "$PWD"
