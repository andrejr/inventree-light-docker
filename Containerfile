ARG VERSION=master
ARG DOCKER_TAG=latest


###############################################
# Base Image
###############################################

FROM python:buster AS python-base

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    SYSTEM_USERNAME=inventree \
    SYSTEM_GROUP=inventree \
    GIT_URL="https://github.com/inventree/InvenTree.git"
ENV HOME_DIR="/home/$SYSTEM_USERNAME"
ENV INVENTREE_ROOT="${HOME_DIR}/src"
ENV INVENTREE_PERSIST="${HOME_DIR}/persist"
ENV APP_PATH="$INVENTREE_ROOT/InvenTree"
ENV VENV_PATH="$INVENTREE_ROOT/.venv"

# prepend venv to path
ENV PATH="$VENV_PATH/bin:$PATH"

# create user
RUN useradd --no-log-init --create-home --shell /bin/bash "$SYSTEM_USERNAME"
USER $SYSTEM_USERNAME
RUN mkdir -p "${INVENTREE_ROOT}"

###############################################
# Builder image
###############################################
FROM python-base AS builder

ARG VERSION=master
ARG DOCKER_TAG=latest

WORKDIR "$INVENTREE_ROOT"

# install deps
USER root
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install --no-install-recommends -y \
    python3 python3-dev python3-pip python3-invoke python3-venv git \
    fontconfig fonts-noto \
    build-essential python3-setuptools python3-wheel \
    python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 \
    libgdk-pixbuf2.0-0 libffi-dev shared-mime-info && \
    fc-cache -f && \
    rm -rf /var/lib/apt/lists/*
USER $SYSTEM_USERNAME

WORKDIR "$INVENTREE_ROOT"

RUN mkdir "$INVENTREE_ROOT/git" && \
    if [ "$DOCKER_TAG" = "latest" ] ; \
    then git clone --branch master --depth 1 "$GIT_URL" git ; \
    else git clone --branch "${VERSION}" --depth 1 "$GIT_URL" git ; fi \
    && find "$INVENTREE_ROOT/git/" -mindepth 1 -maxdepth 1 -exec mv -t ${INVENTREE_ROOT} -- {} + \
    && rm -rf "$INVENTREE_ROOT/git" \
    && python3 -m venv "$VENV_PATH" \
    && . "$VENV_PATH/bin/activate" \
    && pip3 install --upgrade pip setuptools wheel invoke \
    && pip3 install --no-cache-dir -U -r "$INVENTREE_ROOT/requirements.txt" gunicorn


WORKDIR ${INVENTREE_HOME}

###############################################
# Builder image
###############################################
FROM python-base AS production

# ARG BRANCH
# ARG COMMIT
# ARG DATE
# ARG URL
# ARG VERSION

# LABEL org.label-schema.schema-version="1.0" \
#     org.label-schema.build-date=$DATE \
#     org.label-schema.vendor="Zeigren" \
#     org.label-schema.name="zeigren/inventree" \
#     org.label-schema.url="https://hub.docker.com/r/zeigren/inventree" \
#     org.label-schema.version=$VERSION \
#     org.label-schema.vcs-url=$URL \
#     org.label-schema.vcs-branch=$BRANCH \
#     org.label-schema.vcs-ref=$COMMIT

USER root

RUN apt-get update -y && apt-get install --no-install-recommends -y \
    git make libjpeg-turbo-progs zlib1g zlib1g-dev libjpeg-dev \
    fontconfig fonts-noto \
    build-essential python3-setuptools python3-wheel \
    python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 \
    libgdk-pixbuf2.0-0 libffi-dev shared-mime-info \
    gettext-base \
    nginx && \
    fc-cache -f && \
    rm -rf /var/lib/apt/lists/*


COPY --from=builder $INVENTREE_ROOT $INVENTREE_ROOT
COPY config.yaml $INVENTREE_ROOT/InvenTree/
COPY generate_secret_key.py entrypoint.sh nginx.conf.template $INVENTREE_ROOT/

RUN chown -R "$SYSTEM_USERNAME:$SYSTEM_GROUP" "$INVENTREE_ROOT/.."

USER "$SYSTEM_USERNAME"
VOLUME "$INVENTREE_PERSIST"

WORKDIR "$INVENTREE_ROOT"

ENV INVENTREE_SECRET_KEY_FILE="$INVENTREE_PERSIST/secret/key"

# SERVER_NAME
ENV LISTENING_PORT=8743
ENV MIGRATE_ON_START=1
ENV ADMIN_USERNAME=admin
ENV ADMIN_EMAIL=test@example.com
#ENV SERVER_NAME

CMD ["/home/inventree/src/entrypoint.sh"]
